"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
from src.img_helper import decode_class_names
def get_model_n_class_list(args):
    # Initializing the model specific processing and files
    model = args["model"]
    class_name_path = "model_data/class_file.name"
    names = decode_class_names(class_name_path)

    return model, names