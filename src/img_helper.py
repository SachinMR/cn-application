"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import numpy as np
import cv2


def _draw_bounding_boxes_yolo(img, bounding_boxes, scores, classes, names):

    def _draw_bounding_box(img, bounding_box, score, cls):
        
        text = names[int(cls)].title()
        (x, y), base = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 0.8, 2)
        color_codes = [[255, 128, 63], [71, 217, 5], [199, 35, 0] , [157, 7, 248] ,[255, 255, 0]]
        x1, y1, x2, y2 = bounding_box[:4]
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
        img = cv2.rectangle(img, (x1, y1), (x2, y2), color_codes[cls], 2)
        img = cv2.rectangle(img, (x1, y1 - y - base),
                            (x1 + x+20, y1+10),
                            color_codes[cls], -1)
        img = cv2.putText(img,
                          text,
                          (x1+10, y1),
                          cv2.FONT_HERSHEY_DUPLEX, 0.8, (0, 0, 0), 2, cv2.LINE_AA)

        return img

    for bounding_box, score, cls in zip(bounding_boxes, scores, classes):
        img = _draw_bounding_box(img, bounding_box, score, cls)

    return img

def _draw_bounding_boxes_tiny(frame, boxes, scores, classes, names, img_shape):
    class_i = 0
    for bbox in boxes:
        text = names[int(classes[class_i])].title()
        color_codes = [[255, 128, 63], [71, 217, 5], [199, 35, 0] , [157, 7, 248] , [255, 255, 0]]
        (x, y), base = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 0.8, 2)
        xmin = (bbox[0]/img_shape[1])*frame.shape[1]
        ymin = (bbox[1]/img_shape[0])*frame.shape[0]
        xmax = (bbox[2]/img_shape[1])*frame.shape[1]
        ymax = (bbox[3]/img_shape[0])*frame.shape[0]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)
        frame = cv2.rectangle(frame, (xmin, ymin - y - base), (xmin + x+20, ymin+10), color_codes[classes[class_i]], -1)
        cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color_codes[classes[class_i]], 2)
        frame = cv2.putText(frame, text, (xmin+10, ymin), cv2.FONT_HERSHEY_DUPLEX, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
        class_i += 1
    return frame

def _draw_bounding_boxes_yolo2(frame, boxes, scores, classes, names, img_shape):   
    class_i = 0                                                                   
    for bbox in boxes:                                                            
        text = names[int(classes[class_i])].title()                           
        color = (225, 128, 63)                                                
        (x, y), base = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 0.8, 2)                                                                     
        xmin, ymin, xmax, ymax = (bbox[0]/img_shape[1])*frame.shape[1], (bbox[1]/img_shape[0])*frame.shape[0], (bbox[2]/img_shape[1])*frame.shape[1], (bbox[3]/img_shape[0])*frame.shape[0]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)                                                                       
        frame = cv2.rectangle(frame, (xmin, ymin - y - base), (xmin + x+20, ymin+10), color, -1)                                                  
        cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color, 2)                                                                                
        frame = cv2.putText(frame, text, (xmin+10, ymin), cv2.FONT_HERSHEY_DUPLEX, 0.8, (0, 0, 0), 2, cv2.LINE_AA)                                
        class_i += 1                                                                                                                              
    return frame


def postprocess_img(img, img_size, bond_boxes=None):
    
    img_h, img_w = img.shape[:2]
    width, height = img_size

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2

    img = img[dh:new_h + dh, dw:new_w + dw, :]
    img_resized = cv2.resize(img, (width, height))

    if bond_boxes is None:
        return img_resized, None
    else:
        bond_boxes = bond_boxes.astype(np.float32)
        bond_boxes[:, [0, 2]] = np.clip((bond_boxes[:, [0, 2]] - dw) / img_scale, 0., width)
        bond_boxes[:, [1, 3]] = np.clip((bond_boxes[:, [1, 3]] - dh) / img_scale, 0., height)

        return img_resized, bond_boxes


def preprocess_img(img, img_size, bond_boxes=None):
    
    img_w, img_h = img_size
    height, width, _ = img.shape

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    img_resized = cv2.resize(img, (new_w, new_h))

    img_paded = np.full(shape=[1, img_h, img_w, 3], dtype=np.uint8, fill_value=127)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2
    img_paded[:, dh:new_h + dh, dw:new_w + dw, :] = img_resized

    if bond_boxes is None:
        return img_paded

    else:
        bond_boxes = np.asarray(bond_boxes).astype(np.float32)
        bond_boxes[:, [0, 2]] = bond_boxes[:, [0, 2]] * img_scale + dw
        bond_boxes[:, [1, 3]] = bond_boxes[:, [1, 3]] * img_scale + dh

        return img_paded, bond_boxes

def decode_class_names(classes_path):
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes

