"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
def get_final_class_count(classes, model):
    arr = [0] * 5
    for i in list(classes):
        if model == "yolov3" or model == "tiny_yolov3" or model == "yolov4":
            arr[i] = arr[i] + 1
    return arr