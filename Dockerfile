FROM arm64v8/ubuntu:18.04
FROM python:3.7-slim-buster
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt install -y libgl1-mesa-glx
RUN apt install -y libglib2.0-0
RUN apt install -y gcc
RUN apt-get install -y ffmpeg

RUN apt-get clean -y 

RUN python3 -m pip install --no-cache-dir --upgrade pip
RUN python3 -m pip install --no-cache-dir opencv-python
RUN python3 -m pip install --no-cache-dir requests
RUN python3 -m pip install --no-cache-dir flask
RUN python3 -m pip install --no-cache-dir flask_ngrok
RUN python3 -m pip install --no-cache-dir pytz
RUN python3 -m pip install --no-cache-dir grpcio grpcio-tools
RUN python3 -m pip install --no-cache-dir backports.zoneinfo

COPY static/ static/
COPY model_data/ model_data/
COPY config config/
COPY templates/ templates/

COPY src src/
WORKDIR /src/grpc_files

CMD python gen.py
WORKDIR /

COPY rtsp-simple-server rtsp-simple-server
COPY rtsp-simple-server.yml rtsp-simple-server.yml

COPY main.py main.py

ENTRYPOINT [ "python3" ,"main.py" ]
